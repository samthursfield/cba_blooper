Changelog
=========

1.0.1
-----

  * Shrink release binary size (2.1MB -> ~800KB)

1.0.0
-----

  * Initial release
