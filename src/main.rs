mod blooper;
mod download;
mod props;
mod state;

use std::fs;

use anyhow::anyhow;
use arg::Args;
use log::*;

#[derive(Args, Debug)]
/// config
/// Read and write device settings.
struct CmdConfig {
}

#[derive(Args, Debug)]
/// download
/// Download layers and loops.
struct CmdDownload {
    #[arg(short = "a", long)]
    /// Download all loops.
    all: bool,

    #[arg(short = "s", long)]
    /// Download a specific loop (session)
    session: Option<u32>,

    #[arg(short = "l", long)]
    /// Download a specific layer (implies `--session`).
    layer: Option<u32>,
}

#[derive(Args, Debug)]
enum Commands {
    Config(CmdConfig),
    Download(CmdDownload),
}

#[derive(Args, Debug)]
///cba_blooper tool
struct MyArgs {
    ///Select subcommand.
    ///
    ///Available subcommands: `config`, `download`
    #[arg(sub)]
    command: Commands,
}

fn command_config() -> Result<(), anyhow::Error> {
    let state = blooper::get_state()?;

    println!("Sessions:");
    println!();
    for s in state.sessions {
        println!("  * {}: tracks {}, length {:?}", s.index, s.tracks, s.length);
    }
    println!();
    println!("Modififers:");
    println!();
    println!("  * Bank A: {:?}", state.bank_a);
    println!("  * Bank B: {:?}", state.bank_b);
    println!();
    println!("Add assist: {}", state.add_assist);
    println!("Stability noise: {}", state.stability_noise);
    Ok(())
}

fn command_download(all: bool, session: Option<u32>, track: Option<u32>) -> Result<(), anyhow::Error> {
    if all {
        if session.is_some() || track.is_some() {
            return Err(anyhow!("Conflicting arguments: specified `--all` with `--session` or `--track`"));
        }
    } else if session.is_none() {
        return Err(anyhow!("Must specify `--session` if `--all` is not passed"));
    }

    blooper::download(session, track, |session, track, data| {
        let filename = format!("{session:02}.{track:02}.wav");
        println!("Writing {filename} ({} bytes)", data.len());
        fs::write(filename, data).unwrap();
    })?;

    Ok(())
}

fn main() {
    env_logger::init();
    debug!("Initialized logging");

    let args = arg::parse_args::<MyArgs>();

    let result = match &args.command {
        Commands::Config(_) => command_config(),
        Commands::Download(args) =>
            command_download(args.all, args.session, args.layer),
    };

    if result.is_err() {
        println!("ERROR: {:?}", result.err().unwrap());
        std::process::exit(1);
    }
}
