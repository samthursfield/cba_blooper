use crate::props::{self, Prop};

use std::sync::mpsc;
use std::time::Duration;

use anyhow::anyhow;
use midir::MidiOutputConnection;
use log::*;
use num_enum::TryFromPrimitive;

#[derive(Clone, Copy, Debug)]
/// Describes a single saved session.
pub struct SessionInfo {
    pub index: u32,
    pub length: Duration,
    pub tracks: u32,
}

#[derive(Clone, Copy, Debug, TryFromPrimitive)]
#[repr(u32)]
pub enum Modifier {
  Dropper = 1,
  Scrambler = 2,
  TrimmerSmooth = 3,
  TrimmerStepped = 4,
  Filter = 5,
  SpeedSmooth = 6,
  SpeedStepped = 7,
  SpeedChromatic = 16,
  Swapper = 8,
  StutterSmooth = 9,
  StutterStepped = 10,
  Stopper = 11,
  PitcherSmooth = 12,
  PitcherStepped = 13,
  StretcherSmooth = 14,
  StretcherStepped = 15,
}

/// Overall device state
#[derive(Debug)]
pub struct BlooperState {
    pub sessions: [SessionInfo; 16],
    pub bank_a: [Modifier; 6],
    pub bank_b: [Modifier; 6],
    pub add_assist: bool,
    pub stability_noise: u32,
}

impl SessionInfo {
    pub fn new(index: u32, size: u32, tracks: u32) -> Result<SessionInfo, anyhow::Error> {
        let millis: u64 = u64::from(size * 10);
        Ok(SessionInfo {
            index,
            length: Duration::from_millis(millis),
            tracks,
        })
    }

    pub fn new_empty() -> SessionInfo {
        SessionInfo { index: 0, length: Duration::from_millis(0), tracks: 0 }
    }
}


/// Message processing for initial device handshake and session+config discovery
pub fn process_handshake_and_discovery(input_rx: &mpsc::Receiver<Prop>, output: &mut MidiOutputConnection) -> Result<BlooperState, anyhow::Error> {
    let mut state = BlooperState {
        add_assist: false,
        stability_noise: 0,
        bank_a: [Modifier::Dropper;6],
        bank_b: [Modifier::Dropper;6],
        sessions: [SessionInfo::new_empty(); 16],
    };
    let mut discover_session: u32 = 1;
    let mut session_read: [bool; 16] = [false; 16];

    debug!("Send 'reset'");
    props::write_prop(output, &[0x0001, 0, 0, 0, 0, 0])?;

    loop {
        let timeout = Duration::from_millis(100);
        let prop_option = props::read_prop(input_rx, timeout);

        if prop_option.is_none() {
            return Err(anyhow!("Timeout"));
        }

        let prop = prop_option.unwrap();
        match prop[0] {
            0x0001 => {
                debug!("Received 'reset' response");
                debug!("Send 'discover session' 1");
                props::write_prop(output, &[0x0008, discover_session, 0, 0, 0, 0])?;
            },
            0x0008 => {
                let session = prop[1];
                let session_index = (prop[1] - 1) as usize;
                debug!("Received 'discover session' {} response", session);

                if session_read[session_index] {
                    debug!("Ignoring duplicate info on session {}", session);
                } else {
                    let size = prop[2];
                    let tracks = prop[3];
                    state.sessions[session_index] = SessionInfo::new(session, size, tracks)?;
                    session_read[session_index] = true;
                    info!("Received info on session {}: size {}, tracks {}", session, size, tracks);

                    discover_session += 1;

                    if discover_session > 16 {
                        debug!("Send 'session discovery complete'");
                        props::write_prop(output, &[0x0009, 0, 0, 0, 0, 0])?;
                    } else {
                        debug!("Send 'discover session' {}", discover_session);
                        props::write_prop(output, &[0x0008, discover_session, 0, 0, 0, 0])?;
                    }
                }
            },
            0x0009 => {
                debug!("Received 'session discovery complete'");
                debug!("Send 'discover config'");
                props::write_prop(output, &[0x000A, 0, 0, 0, 0, 0])?;
            },
            #[allow(clippy::identity_op)]
            0x000A => {
                debug!("Received 'discover config'");
                state.bank_a[0] = Modifier::try_from((((prop[5] >>  0) & 3) << 4) + ((prop[1] >>  0) & 15))?;
                state.bank_a[1] = Modifier::try_from((((prop[5] >>  2) & 3) << 4) + ((prop[1] >>  4) & 15))?;
                state.bank_a[2] = Modifier::try_from((((prop[5] >>  4) & 3) << 4) + ((prop[1] >>  8) & 15))?;
                state.bank_a[3] = Modifier::try_from((((prop[5] >>  6) & 3) << 4) + ((prop[1] >> 12) & 15))?;
                state.bank_a[4] = Modifier::try_from((((prop[5] >>  8) & 3) << 4) + ((prop[1] >> 16) & 15))?;
                state.bank_a[5] = Modifier::try_from((((prop[5] >> 10) & 3) << 4) + ((prop[1] >> 20) & 15))?;

                state.bank_b[0] = Modifier::try_from((((prop[5] >> 12) & 3) << 4) + ((prop[2] >>  0) & 15))?;
                state.bank_b[1] = Modifier::try_from((((prop[5] >> 14) & 3) << 4) + ((prop[2] >>  4) & 15))?;
                state.bank_b[2] = Modifier::try_from((((prop[5] >> 16) & 3) << 4) + ((prop[2] >>  8) & 15))?;
                state.bank_b[3] = Modifier::try_from((((prop[5] >> 18) & 3) << 4) + ((prop[2] >> 12) & 15))?;
                state.bank_b[4] = Modifier::try_from((((prop[5] >> 20) & 3) << 4) + ((prop[2] >> 16) & 15))?;
                state.bank_b[5] = Modifier::try_from((((prop[5] >> 22) & 3) << 4) + ((prop[2] >> 20) & 15))?;

                if prop[3] != 0 {
                    state.stability_noise = prop[3];
                }
                if prop[4] == 0 {
                    state.add_assist = false;
                } else {
                    state.add_assist = true;
                }

                break;
            },
            _ => { warn!("Unexpected opcode: {}", prop[0]) },
        }
    }

    Ok(state)
}
