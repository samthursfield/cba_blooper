use crate::props::{self, Prop};
use crate::state::{BlooperState, SessionInfo};

use std::sync::mpsc;
use std::time::Duration;

use anyhow::anyhow;
use log::*;
use midir::MidiOutputConnection;

fn push_u16le(a: &mut Vec<u8>, x: u16) {
    a.push((x & 255).try_into().unwrap());
    a.push(((x >> 8) & 255).try_into().unwrap());
}

fn push_u32le(a: &mut Vec<u8>, x: u32) {
    a.push((x & 255).try_into().unwrap());
    a.push(((x >> 8) & 255).try_into().unwrap());
    a.push(((x >> 16) & 255).try_into().unwrap());
    a.push(((x >> 24) & 255).try_into().unwrap());
}

fn samples_to_wave(samples: Vec<u32>) -> Vec<u8> {
    let mut data: Vec<u8> = Vec::with_capacity(44 + 3 * samples.len());
    let samples_len: u32 = samples.len().try_into().unwrap();

    push_u32le(&mut data, 0x46464952); // 'RIFF'
    push_u32le(&mut data, 36 + 3 * samples_len); // Total size
    push_u32le(&mut data, 0x45564157); // 'WAVE'

    push_u32le(&mut data, 0x20746D66); // 'fmt '
    push_u32le(&mut data, 16); // Block size
    push_u16le(&mut data, 1); // Format
    push_u16le(&mut data, 1); // Channels
    push_u32le(&mut data, 32000); // Rate
    push_u32le(&mut data, samples_len); // Count
    push_u16le(&mut data, 3); // 3 byte word alignment
    push_u16le(&mut data, 24); // 24 bits per sample

    push_u32le(&mut data, 0x61746164); // 'data'
    push_u32le(&mut data, 3 * samples_len); // Block size

    for sample in 0..samples.len() {
        data.push((sample & 255).try_into().unwrap());
        data.push(((sample >> 8) & 255).try_into().unwrap());
        data.push(((sample >> 16) & 255).try_into().unwrap());
    }
    data
}

fn session_empty(session_infos: &[SessionInfo; 16], session: u32) -> bool {
    let session_index: usize = (session - 1) as usize;
    session_infos[session_index].length == Duration::from_secs(0)
}

fn track_empty(session_infos: &[SessionInfo; 16], session: u32, track: u32) -> bool {
    let session_index: usize = (session - 1) as usize;
    track > session_infos[session_index].tracks
}

// Track state of process_download() function.
struct DownloadState {
    session: u32,
    track: u32,
    data: Vec<u32>,
    length: Option<usize>,
}

enum NextDownloadResult {
    NextTrack,
    NextSession,
    Complete,
}

impl DownloadState {
    fn new(session_infos: &[SessionInfo; 16], selected_session: Option<u32>, selected_track: Option<u32>) -> Result<Self, anyhow::Error> {
        let session_range = 1..16;
        if let Some(session) = selected_session {
            if !(session_range.contains(&session)) {
                return Err(anyhow!("Session out of range {session_range:#?}: {}", session));
            }

            if session_empty(session_infos, session) {
                return Err(anyhow!("Session {} is empty", session));
            }
        }

        let track_range = 1..8;
        if let Some(track) = selected_track {
            if !(track_range.contains(&track)) {
                return Err(anyhow!("Track out of range {track_range:#?}: {}", track));
            }

            if track_empty(session_infos, selected_session.unwrap(), track) {
                return Err(anyhow!("Session {} track {} is empty", selected_session.unwrap(), track));
            }
        }

        let result = DownloadState {
            session: selected_session.unwrap_or(1),
            track: selected_track.unwrap_or(1),
            data: vec!(),
            length: None,
        };
        Ok(result)
    }

    fn next(&mut self, session_infos: &[SessionInfo; 16], session: Option<u32>, track: Option<u32>) -> NextDownloadResult {
        if track.is_some() {
            return NextDownloadResult::Complete;
        }

        let session_info = session_infos[self.session as usize - 1];
        if self.track < session_info.tracks {
            self.track += 1;
            return NextDownloadResult::NextTrack;
        }

        if session.is_some() {
            return NextDownloadResult::Complete;
        }

        self.track = 1;
        self.session += 1;
        while self.session <= 16 && session_empty(session_infos, self.session) {
            self.session += 1;
        }

        if self.session > 16 {
            return NextDownloadResult::Complete;
        }

        NextDownloadResult::NextSession
    }
}

// Message processing for session download.
pub fn process_download<F>(input_rx: &mpsc::Receiver<Prop>,
                           output: &mut MidiOutputConnection,
                           state: BlooperState,
                           session: Option<u32>,
                           track: Option<u32>,
                           data_callback: F) -> Result<i32, anyhow::Error>
where F: Fn(u32, u32, Vec<u8>)
{
    let mut result_count = 0;
    let mut dl = DownloadState::new(&state.sessions, session, track)?;

    debug!("Send 'get track length' {} {:?}", dl.session, dl.track);
    props::write_prop(output, &[0x0005, dl.session, dl.track, 0, 0, 0])?;

    loop {
        let timeout = Duration::from_millis(100);
        let prop_option = props::read_prop(input_rx, timeout);

        if prop_option.is_none() {
            return Err(anyhow!("Timeout"));
        }

        let prop = prop_option.unwrap();
        match prop[0] {
            0x0005 => {
                dl.length = Some(prop[1].try_into().unwrap());
                debug!("Received 'get track length' response: {:?}", dl.length);
                if dl.length == Some(0) {
                    debug!("Track has length zero, waiting for 'download complete' opcode");
                } else {
                    debug!("Send 'download track' {}", dl.track);
                    props::write_prop(output, &[0x0006, dl.track, 0, 0, 0, 0])?;
                }
            },
            0x0006 => {
                debug!("Received 'download track' response (dl.track {}, existing data {} / {}).",
                       dl.track, dl.data.len(), dl.length.unwrap());
                if prop.len() != 34 {
                    return Err(anyhow!("Expected property of length 34, received length {}", prop.len()));
                }
                for dword in prop.iter().take(33).skip(1) {
                    dl.data.push(*dword);
                    if dl.data.len() >= dl.length.unwrap() {
                        break;
                    }
                }
                if dl.data.len() < dl.length.unwrap() {
                    debug!("Send 'download track' {}", dl.track);
                    props::write_prop(output, &[0x0006, dl.track, 0, 0, 0, 0])?;
                } else {
                    debug!("Send 'download complete'");
                    props::write_prop(output, &[0x0007, 0, 0, 0, 0, 0])?;
                }
            },
            0x0007 => {
                debug!("Received 'download complete'");
                data_callback(dl.session, dl.track, samples_to_wave(dl.data));
                result_count += 1;

                dl.length = None;
                dl.data = vec!();

                let next_result = dl.next(&state.sessions, session, track);
                match next_result {
                    NextDownloadResult::NextTrack => {
                        debug!("Moving to next track: {}", dl.track);
                    },
                    NextDownloadResult::NextSession => {
                        debug!("Moving to next session: {}", dl.session);
                        debug!("Send 'reset'");
                        props::write_prop(output, &[0x0001, 0, 0, 0, 0, 0])?;
                    },
                    NextDownloadResult::Complete => {
                        debug!("Download complete");
                        debug!("Send 'reset'");
                        props::write_prop(output, &[0x0001, 0, 0, 0, 0, 0])?;
                        break;
                    },
                }

                debug!("Send 'get track length' {} {:?}", dl.session, dl.track);
                props::write_prop(output, &[0x0005, dl.session, dl.track, 0, 0, 0])?;
            },
            _ => { warn!("Unexpected opcode: {}", prop[0]) },
        }
    }

    Ok(result_count)
}
