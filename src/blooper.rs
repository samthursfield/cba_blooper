use crate::download;
use crate::props::{self, Prop};
use crate::state::{self, BlooperState};

use std::sync::mpsc;

use anyhow::Context;
use midir::{MidiInput, MidiInputPort, MidiOutput, MidiOutputConnection, MidiOutputPort};
use log::*;

fn find_blooper_input_port(midi_in: &MidiInput) -> Option<MidiInputPort> {
    for (i, p) in midi_in.ports().iter().enumerate() {
        let name = midi_in.port_name(p).unwrap();
        debug!("Considering input port {}: {}", i, name);
        if name.starts_with("Blooper") {
            return Some(p.clone());
        }
    }
    None
}

fn find_blooper_output_port(midi_out: &MidiOutput) -> Option<MidiOutputPort> {
    for (i, p) in midi_out.ports().iter().enumerate() {
        let name = midi_out.port_name(p).unwrap();
        debug!("Considering output port {}: {}", i, name);
        if name.starts_with("Blooper") {
            return Some(p.clone());
        }
    }
    None
}

// "Context manager" to set up Blooper communication and run `callback`.
fn with_blooper<F, T>(callback: F) -> Result<T, anyhow::Error>
where F: FnOnce(&mpsc::Receiver<Prop>, &mut MidiOutputConnection) -> Result<T, anyhow::Error>
{
    let midi_in = MidiInput::new("cba_blooper input").unwrap();
    let midi_in_port = find_blooper_input_port(&midi_in)
        .context("Blooper MIDI input wasn't detected.")?;

    let midi_out = MidiOutput::new("cba_blooper output").unwrap();
    // Not needed, according to https://docs.rs/midir/latest/midir/struct.MidiInput.html
    //midi_in.ignore(Ignore::None);

    let midi_out_port = find_blooper_output_port(&midi_out)
        .context("Blooper MIDI output wasn't detected.")?;

    let (input_tx, input_rx) = mpsc::sync_channel::<Prop>(100);
    let _midi_in_connection = midi_in.connect(&midi_in_port, "cba_blooper", move |stamp, message, _| {
        debug!("MIDI message: {}: {:?} (len = {})", stamp, message, message.len());
        let maybe_prop = props::midi_to_prop(message);
        if let Some(prop) = maybe_prop {
            input_tx.send(prop).unwrap();
        }
    }, ());

    let mut output = midi_out.connect(&midi_out_port, "cba_blooper").unwrap();

    callback(&input_rx, &mut output)
}

/// Query and return device state.
pub fn get_state() -> Result<BlooperState, anyhow::Error> {
    with_blooper(|input_rx, output| {
        let state = state::process_handshake_and_discovery(input_rx, output)?;
        Ok(state)
    })
}

/// Download sessions (loops) and individual tracks.
///
/// If `session` is None, all sessions are downloaded. Otherwise, `session` selects a specific
/// session from 1 to 16
///
/// If `track` is None, all layers of the specified session(s) are downloaded. Otherwise, `track`
/// selects a specific layer from 1 to 8.
///
/// For each track, `data_callback` will be called with the session number, the track number, and
/// the 24-bit WAV audio data in a `Vec<u8>`.
pub fn download<F>(session: Option<u32>, track: Option<u32>, data_callback: F) -> Result<i32, anyhow::Error>
where F: Fn(u32, u32, Vec<u8>)
{
    with_blooper(move |input_rx, output| {
        let state = state::process_handshake_and_discovery(input_rx, output)?;
        let result_count = download::process_download(input_rx, output, state, session, track, data_callback)?;
        Ok(result_count)
    })
}
