//! Properties, the unit of communication between host and blooper.

use std::sync::mpsc::{self, RecvTimeoutError};
use std::time::Duration;

use midir::{MidiOutputConnection};
use log::*;

/// A prop is a list of u32. It usually has 6 x u32. For data transfer
/// it can have 37 x u32 instead.
pub type Prop = Vec<u32>;

/// Read 32-bit value in 4 bit increments.
fn read_nybble(value: u32, pos: u8) -> u8 {
    ((value >> (pos * 4)) & 15).try_into().unwrap()
}

/// Write 32-bit value in 4 bit increments 
fn write_nybble(value: u8, pos: u8) -> u32 {
    u32::from(value) << (pos * 4)
}

pub fn midi_to_prop(midi_data: &[u8]) -> Option<Prop> {
    if midi_data.len() >= 49 && midi_data[0] == 0xF0 && midi_data[49] == 0xF7 {
        let mut property: Prop = vec![0; 6];
        for ii in 0..6 {
            property[ii] =
                write_nybble(midi_data[8*ii+1], 7) + write_nybble(midi_data[8*ii+2], 6) +
                write_nybble(midi_data[8*ii+3], 5) + write_nybble(midi_data[8*ii+4], 4) +
                write_nybble(midi_data[8*ii+5], 3) + write_nybble(midi_data[8*ii+6], 2) +
                write_nybble(midi_data[8*ii+7], 1) + write_nybble(midi_data[8*ii+8], 0);
        }
        return Some(property);
    }
    else if midi_data.len() >= 266 && midi_data[0] == 0xF0 && midi_data[265] == 0xF7 {
        let mut property: Prop = vec![0; 34];
        // Note: in the BLIP code, the loop goes to 1+36 (37). This actually overflows the
        // input buffer which is 266 bytes long. We only need to read 33 qwords, corresponding
        // to slice `midi_data[1:265]`.
        for ii in 0..33 {
            property[ii] =
                write_nybble(midi_data[8*ii+1], 7) + write_nybble(midi_data[8*ii+2], 6) +
                write_nybble(midi_data[8*ii+3], 5) + write_nybble(midi_data[8*ii+4], 4) +
                write_nybble(midi_data[8*ii+5], 3) + write_nybble(midi_data[8*ii+6], 2) +
                write_nybble(midi_data[8*ii+7], 1) + write_nybble(midi_data[8*ii+8], 0);
        }
        return Some(property);
    }
    None
}

pub fn prop_to_midi(property: &[u32;6]) -> Vec<u8> {
    let mut midi_data: Vec<u8> = vec!(0xF0; 1);
    for p in property.iter() {
        midi_data.push(read_nybble(*p, 7)); midi_data.push(read_nybble(*p, 6));
        midi_data.push(read_nybble(*p, 5)); midi_data.push(read_nybble(*p, 4));
        midi_data.push(read_nybble(*p, 3)); midi_data.push(read_nybble(*p, 2));
        midi_data.push(read_nybble(*p, 1)); midi_data.push(read_nybble(*p, 0));
    }
    midi_data.push(0xF7);
    midi_data
}

// Read a 6 or 36-qword property from Prop receiver channel `rx`.
pub fn read_prop(rx: &mpsc::Receiver<Prop>, timeout: Duration) -> Option<Prop> {
    return match rx.recv_timeout(timeout) {
        Ok(data) => Some(data),
        Err(RecvTimeoutError::Timeout) => None,
        Err(RecvTimeoutError::Disconnected) => panic!("Read channel was closed unexpectedly"),
    }
}

// Write a 6-qword property to MIDI output connection at `output`.
pub fn write_prop(output: &mut MidiOutputConnection, prop: &[u32; 6]) -> Result<(), anyhow::Error> {
    let midi_data = prop_to_midi(prop);
    std::thread::sleep(std::time::Duration::from_millis(10));
    debug!("Send: {:?}", midi_data);
    output.send(&midi_data)?;
    Ok(())
}
