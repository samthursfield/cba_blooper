# `cba_blooper`: Blooper commandline tool

This is a commandline interface for the Chase Bliss Audio [blooper pedal](https://www.chasebliss.com/blooper).
It's based on the [B.L.I.P. tool](https://chasebliss.github.io/), supporting a subset of its functionality.

The goals of this project are:

  * allow copying all audio from the blooper with a single command
  * have fun coding things in Rust

## Installation

The tool is currently available for Linux/GNU.

You can download the latest release from the [Gitlab package registry](https://gitlab.com/samthursfield/cba_blooper/-/packages/). It's a standalone binary which you can run in place, or copy to somewhere in `$PATH` such as the `.local/bin` directory inside your homedir.

## Usage

To show device status:

    cba_blooper config

To download a session:

    cba_blooper download --session 1

Add `--track` to get only a single track.

To download all sessions:

    cba_blooper download --all

## Happy blooping

![Blooper](docs/blooper-front.png)
